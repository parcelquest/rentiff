/******************************************************************


******************************************************************/

#ifdef WIN32
#define _export
#endif

/* Definitions */
#define IDM_ABOUT           100
#define IDM_FILE            112
#define IDM_PREVIOUS        113
#define IDM_NEXT            114
#define IDM_LIGHT           118
#define IDM_DARK            119
#define IDM_FULL            120
#define IDM_ZOOMIN          123
#define IDM_ZOOMOUT         124
#define IDM_ANNOT           125
#define IDM_PRINT           126
#define IDM_PRINTOPT        127
#define IDM_UP              180
#define IDM_DOWN            181
#define IDM_LEFT            182
#define IDM_RIGHT           183

#define IDM_VIEWFITHORZ     2002
#define IDM_VIEWFITVERT     2003
#define IDM_VIEWICON        2004
#define IDM_VIEWDARK        2005
#define IDM_VIEWLIGHT       2006
#define IDM_VIEWREVERSE     2007
#define IDM_VIEWBACKBLACK   2008
#define IDM_VIEWBACKWHITE   2009
#define IDM_VIEW1TO1        2010
#define IDM_VIEWGRAY        2011
#define IDM_VIEWMONO        2012

#define IDM_ROTRIGHT        3001
#define IDM_ROTLEFT         3002
#define IDM_ROT0            3003
#define IDM_ROT90           3004
#define IDM_ROT180          3005
#define IDM_ROT270          3006

#define IDM_COPY            401    

/* Parameter values */
#define DIS_FULL_MAP		1
#define DIS_UPPER_LEFT		2
#define DIS_UPPER_RIGHT		3
#define DIS_LOWER_LEFT		4
#define	DIS_LOWER_RIGHT		5

#define ROT_UPRIGHT			11
#define ROT_RIGHT			12
#define ROT_LEFT			13
#define ROT_UPSIDEDOWN		14

#define TYP_MAPS			21
#define TYP_RECORDER		22


/* Prototypes */
int PASCAL WinMain(HINSTANCE, HINSTANCE, LPSTR, int);
BOOL InitApplication(HINSTANCE);
BOOL InitInstance(HINSTANCE, int);
long CALLBACK _export MainWndProc(HWND, UINT, WPARAM, LPARAM);
BOOL _export CALLBACK About(HWND, unsigned, WPARAM, LPARAM);
BOOL _export CALLBACK Parameters(HWND, unsigned, WPARAM, LPARAM);
BOOL _export CALLBACK RenameFile(HWND, unsigned, WPARAM, LPARAM);
// BOOL _export CALLBACK AdvPrint(HWND, unsigned, WORD, LONG);
// BOOL _export CALLBACK CloseBox(HWND, unsigned, WORD, LONG);
