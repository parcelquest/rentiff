#ifdef WIN32

#include "decomp32.h"

#else

#define WIN16

#ifdef WIN16

extern "C" {

short FAR PASCAL DecompUpdateTIFFTag(char far *filename,
                                  short page, short TagNumber,
                                  long NumVal, char far *StringVal);
short FAR PASCAL DecompOpenTIFF(char far *,short);
short FAR PASCAL DecompOpenFile(char far *,long,short,short,short,short,short);
short FAR PASCAL DecompOpenMemory(char far *,short,short,short,short,short,short,short);
void FAR PASCAL DecompClose(short);
void FAR PASCAL DecompSetViewRect(short,short,short,short,short);
void FAR PASCAL DecompSetViewRectDC(short,short,short,short,short);
void FAR PASCAL DecompViewRect(short,short);
short  FAR PASCAL DecompUpdateView(short,short);
void FAR PASCAL DecompScroll(short handle, short Direction, short Amount);
void FAR PASCAL DecompZoom(short, short);
void FAR PASCAL DecompContrast(short, short);
void FAR PASCAL DecompReverse(short,short);
short  FAR PASCAL DecompCopyImage(short);
void FAR PASCAL DecompOutput(short,short,int,int,int);
void FAR PASCAL DecompOutputGreyScale(short,short,int,int,int);
void FAR PASCAL DecompRotation(short, short);
void FAR PASCAL DecompRect(short,short,short,short,short);
void FAR PASCAL DecompRectNoScaling(short,short,short,short,short);
void FAR PASCAL DecompZoomRect(short,short,short,short,short);
void FAR PASCAL DecompEOLFill(short,unsigned char);

void FAR PASCAL DecompHorzScale(short,short,short,short);
void FAR PASCAL DecompVertScale(short,short,short,short);

void FAR PASCAL DecompGetView(short,struct CurrentView far *);
void FAR PASCAL DecompGetImage(short,struct ImageAttributes far *);
void FAR PASCAL DecompGetOutputInfo(short,struct OutputInfo far *);

short FAR PASCAL DecompRAPIDPrintWin(short handle, short mode,short xstart, short ystart, short form, int port);

short FAR PASCAL DecompScrollGetVertMax(short handle);
short FAR PASCAL DecompScrollGetHorzMax(short handle);

unsigned short FAR PASCAL DecompScrollGetVertPos(short handle);
unsigned short FAR PASCAL DecompScrollGetHorzPos(short handle);

unsigned short FAR PASCAL DecompVertScaledSize(short handle);
unsigned short FAR PASCAL DecompHorzScaledSize(short handle);

short FAR PASCAL DecompGetScaledLineSize(short handle);
short FAR PASCAL DecompGetScaledRowsSize(short handle);
short FAR PASCAL DecompGetBitmapLines(short handle,
                                    short InStartRow,
                                    short InNumberLines,
                                    char far *OutBuffer,
                                    short OutBufferSize);
short FAR PASCAL DecompGetRunLines(short handle,
                                 short InStartRow,
                                 short far *OutBuffer);
short FAR PASCAL DecompGetRunCount(short handle,
                                    short InStartRow);
void FAR PASCAL DecompScaling(short handle,short value);

short FAR PASCAL DecompGetPrintLines(short handle,
                                    short InStartRow,
                                    short InNumberLines,
                                    char far *OutBuffer,
                                    short OutBufferSize);
short FAR PASCAL DecompGetPrintVertSize(short handle);
short FAR PASCAL DecompGetPrintHorzSize(short handle);

short FAR PASCAL DecompInUse(short handle);
short FAR PASCAL DecompRAPIDAvail(void);
void FAR PASCAL DecompSetOrigins(short degree,short Column,short Row);

};

#endif

/* - ALL Win32 defines come from decomp32.h

#ifdef WIN32
#define DllExport __declspec( dllexport)
short DllExport DecompUpdateTIFFTag(char *filename,
                                  short page, short TagNumber,
                                  long NumVal, char *StringVal);
short DllExport DecompOpenTIFF(char *,short);
short DllExport DecompOpenFile(char *,long,short,short,short,short,short);
short DllExport DecompOpenMemory(char *,short,short,short,short,short,short,short);
void DllExport DecompClose(short);
void DllExport DecompSetViewRect(short,short,short,short,short);
void DllExport DecompSetViewRectDC(short,short,short,short,short);
void DllExport DecompViewRect(short,short);
short  DllExport DecompUpdateView(short,short);
void DllExport DecompScroll(short handle, short Direction, short Amount);
void DllExport DecompZoom(short, short);
void DllExport DecompContrast(short, short);
void DllExport DecompReverse(short,short);
short  DllExport DecompCopyImage(short);
void DllExport DecompOutput(short,short,int,int,int);
void DllExport DecompOutputGreyScale(short,short,int,int,int);
void DllExport DecompRotation(short, short);
void DllExport DecompRect(short,short,short,short,short);
void DllExport DecompRectNoScaling(short,short,short,short,short);
void DllExport DecompZoomRect(short,short,short,short,short);
void DllExport DecompEOLFill(short,unsigned char);

void DllExport DecompHorzScale(short,short,short,short);
void DllExport DecompVertScale(short,short,short,short);

void DllExport DecompGetView(short,struct CurrentView *);
void DllExport DecompGetImage(short,struct ImageAttributes *);
void DllExport DecompGetOutputInfo(short,struct OutputInfo *);

short DllExport DecompRAPIDPrintWin(short handle, short mode,short xstart, short ystart, short form, int port);

short DllExport DecompScrollGetVertMax(short handle);
short DllExport DecompScrollGetHorzMax(short handle);

unsigned short DllExport DecompScrollGetVertPos(short handle);
unsigned short DllExport DecompScrollGetHorzPos(short handle);

unsigned short DllExport DecompVertScaledSize(short handle);
unsigned short DllExport DecompHorzScaledSize(short handle);

short DllExport DecompGetScaledLineSize(short handle);
short DllExport DecompGetScaledRowsSize(short handle);
short DllExport DecompGetBitmapLines(short handle,
                         short InStartRow,
                         short InNumberLines,
                         char *OutBuffer,
                         short OutBufferSize);
short DllExport DecompGetRunLines(short handle,
                      short InStartRow,
                      short *OutBuffer);
short DllExport DecompGetRunCount(short handle,
                         short InStartRow);
void DllExport DecompScaling(short handle,short value);

short DllExport DecompGetPrintLines(short handle,
                         short InStartRow,
                         short InNumberLines,
                         char *OutBuffer,
                         short OutBufferSize);
short DllExport DecompGetPrintVertSize(short handle);
short DllExport DecompGetPrintHorzSize(short handle);

short DllExport DecompInUse(short handle);
short DllExport DecompRAPIDAvail(void);
void DllExport DecompSetOrigins(short degree,short Column, short Row);
#endif
*/


/* defines */
#define INITDISPLAYHANDLE  32

#define OPENMEMINIT      0x0001
#define OPENMEMDATA      0x0002
#define OPENMEMLASTBLOCK 0x0004

#define STATIC             0

#define FAXLOWRES          0x0001
#define BITSEX             0x0002
#define INITDISPHORZ       0x0004
#define INITDISPVERT       0x0008
#define MIRROR             0x0010

#define REPAINT            0
#define FORCEREPAINT       1

#define FITHORZ            1
#define FITVERT            2
#define FITPAGE            6
#define FITPAGESHRINK      5
#define FITICON            7
#define FILLPAGE           8

#define UP                 0
#define DOWN               1
#define LEFT               2
#define RIGHT              3
#define VERT               4
#define HORZ               5
#define VERTSCALED         6
#define HORZSCALED         7

#define ZOOMIN             0
#define ZOOMOUT            1

#define LIGHTER            0
#define DARKER             1
#define LIGHT              2
#define MEDIUM             3
#define DARK               4
#define DARKEST            5

#define REVERSEOFF         1
#define REVERSEON          2

#define ROTATION_0         0
#define ROTATION_90        1
#define ROTATION_180       2
#define ROTATION_270       3
#define ROTATE_LEFT        4
#define ROTATE_RIGHT       5
//
// output device types
// as used in deccalls.c
//
// VALUES for DecompOutput()
//
#define OUTPUTWIN16      1
#define OUTPUTWINDOWS     1
#define OUTPUTWIN16GREY  2
#define OUTPUTWINDOWSGREY  2
//
// for normal output to HDC
//
#define OUTPUTWIN16HDC   3
#define OUTPUTWIN16HDCG  6
//
// The following outputs are for more exact windowing
// especially for small portions of a invalidated image
// First set is for Window Handle as input from decompoutput
#define OUTPUTWIN16OFFG  4
#define OUTPUTWINDOWSOFFG  4
#define OUTPUTWIN16OFFM  5
#define OUTPUTWINDOWSOFFM  5
// This set if for HDC Handle as input from decompoutput
#define OUTPUTHDCOFFG  7
#define OUTPUTHDCOFFM  8


#define OUTPUTLASER        50
#define OUTPUTGENWINPRINT  51
#define OUTPUTRAPIDDOS     101  /* RESERVED */
#define OUTPUTRAPIDWIN     101  /* RESERVED */

#define VGAC               640
#define VGAR               480

#define LPT1               0
#define LPT2               1

#define HPLASER150C        1200
#define HPLASER150R        1575

#define HPLASER300C        2400
#define HPLASER300R        3150

#define BLACK              0x00
#define WHITE              0xFF

/* RAPID Print defines */
#define SLOW               0   /* 300 dpi */
#define FAST               1   /* 150 dpi */
#define SUPERSLOW          2   /* 600 dpi */
#define DPI300             0   /* 300 dpi */
#define DPI150             1   /* 150 dpi */
#define DPI600             2   /* 600 dpi */

/* Scale to Grey defines */
#define SCALETOBINARY      0
#define SCALETOGREY        1
#define SCALETOGGLE        2

/* defines for compression */
#define UNCOMPRESSED       1
#define GROUP3EOLS         2
#define GROUP3FAX          3
#define GROUP4             4
#define GROUP32            5
#define PACKBITS           6
#define PCX                7
#define MMR                8
#define UNCOMPRESSEDBLACK  9

/* DllExport DecompSetOrigins defines */
#define FROMSTARTCOLUMN    0
#define FROMENDCOLUMN      1
#define FROMSTARTROW       0
#define FROMENDROW         1


/* end defines */

/*****************************************************/
/* Structures                                        */
/*****************************************************/
typedef struct CurrentView {

   unsigned short StartRow;
   unsigned short EndRow;
   unsigned short StartColumn;
   unsigned short EndColumn;
   unsigned short ScaleHorz;
   unsigned short ScaleHorzTenths;
   unsigned short ScaleHorzExpand;
   unsigned short ScaleVert;
   unsigned short ScaleVertTenths;
   unsigned short ScaleVertExpand;
   unsigned short ScaledBytesPerLine;
   unsigned short ScaledRows;
   unsigned short Rotation;
   unsigned short Contrast;
   unsigned short ScalingType;
   short OldStartRow;
   short OldEndRow;
   short OldStartColumn;
   short OldEndColumn;
} ViewInformation;

typedef struct ImageAttributes {

   unsigned short ImageWidth;
   unsigned short ImageLength;
   unsigned short Compression;
   unsigned short FillOrder;
   unsigned short DpiX;
   unsigned short DpiY;
   unsigned short StrippedFlag;
   unsigned short CompressedFileSize;
   unsigned short CompressionRatio;
   unsigned short InputImageLength;
} ImageInformation;

typedef struct OutputInfo {

   short StartColumn;
   short EndColumn;
   short StartRow;
   short EndRow;
   short ScreenWidth;
   short ScreenWidthBytes;
   short ScreenLength;

} OutputInformation;

typedef struct LastView {
   short StartRow;
   short EndRow;
   short StartColumn;
   short EndColumn;
} LastViewInformation;

typedef struct TIFFInfo {

   unsigned short ImageWidth;
   unsigned short ImageLength;
   unsigned short Compression;
   unsigned short FillOrder;
   unsigned short DpiX;
   unsigned short DpiY;
   long         NextIFDOffset;
   long         CompressedData;
   long         StripByteCount;
   long         NextIFDOffsetAddress;

} TIFFInformation;

#ifdef WIN16
short FAR PASCAL DecompTIFFPageInfo(char far *f,short page, struct TIFFInfo far *Info);
#else
short DllExport DecompTIFFPageInfo(char *f,short page, struct TIFFInfo *Info);
#endif
#ifdef WIN16
short FAR PASCAL DecompTIFFNumPages(char far *filename);
#else
short DllExport DecompTIFFNumPages(char *filename);
#endif
#ifdef WIN16
typedef struct tagOFFSET
{
    short xoffset;
    short yoffset;
} OFFSET;
typedef OFFSET FAR*  LPOFFSET;
#endif

typedef struct TIFFMultiPage {
   unsigned int ImageWidth;
   unsigned int ImageLength;
   unsigned int Compression;
   unsigned int FillOrder;
   unsigned int DpiX;
   unsigned int DpiY;
   long         CompressedData;
   long         StripByteCount;
} TIFFMULTI;

#ifdef WIN16
int FAR PASCAL DecompTIFFMultiPageInfo(char far *, struct TIFFMultiPage far *);
#else
int DllExport DecompTIFFMultiPageInfo(char *filename, struct TIFFMultiPage *);
#endif

#endif // WIN32
