/****************************************************************************
  
    PROGRAM:    rentiff.cpp

    PURPOSE:    Main program for CD-Data Rename TIFF program

    FUNCTIONS:

    WinMain() - calls initialization function, processes message loop
    InitApplication() - initializes window data and registers window
    InitInstance() - saves instance handle and creates main window 
    MainWndProc() - processes messages
    About() - processes messages for "About" dialog box
    CloseBox() - processes messages for "CloseBox" dialog box
    DisplayPage( ) - display image in main window
    ReadFile( ) - use common dialog box to get file name
    GetImageSize( ) - calculate size of scaled image
    SetScrollRanges( ) - set oroginal scroll bar ranges
    ResetScrollBars( ) - reset scroll bar dimensions after size
    NormalizeRect( ) - normalize rectangle dimensions
    TrackMouse( ) - start mouse tracking
    DrawSelect( ) - draw the rubber rectangle
    PrintFile( ) - print the currently displayed image

    COMMENTS:

        Windows can have several copies of your application running at the
        same time.  The variable hInst keeps track of which instance this
        application is so that processing will be to the correct window.

****************************************************************************/

#include "windows.h"            /* Required for all Windows applications */
#include "commdlg.h"
#ifndef WIN32
#include "print.h"
#endif
#include "rentiff.h"            /* Specific to this program */
#include "resource.h"

#define WINDOWS
#include "decomp3.h"                                     
#include <ctype.h>
#include <direct.h>
#include <stdio.h>
#include <io.h>
#include <stdlib.h>   
#include <string.h>
#include <time.h>

HINSTANCE     	hInst;              /* Current instance */
HANDLE      	hAccel;             /* Accelerator table */
HWND        	hWndMain;           /* Handle to Main Window */
// HMENU       hMenu;              /* Handle to Main Menu */

// int         PrintSelection = 0; // print selection 0 = normal
                                //       selection 1 = selection
// int         PrintScaling = 0;   // print scaling 0 = fit to page
                                //       scaling 1 = original/actual size (noscaling)
int				ImageHandle = -1;   /* Handle to current image */
// char        ReqName[64];        /* Request file name */
#define FBUFSIZE 512
char        OpenName[_MAX_PATH];    /* File name to display */
char        RequestDir[_MAX_PATH];  /* Name of request directory */
char        SaveImgDir[_MAX_PATH];  /* Name of bad image directory */
HCURSOR     hCursor;                /* Handle to current cursor */
RECT        XRect;                  /* Window dimensions */
HWND        hwnd;                   /* Handle to main window */    

WORD			nDisplay = DIS_FULL_MAP;	/* Display parameter */
WORD			nRotate = ROT_RIGHT;		/* Rotation parameter */
WORD			nDocType = TYP_MAPS;		/* Document type parameter */
BOOL			bAutoNum = TRUE;			/* Automatic numbering */
char			FileExt[10]= "*.*";

POINT       ptSize;             /* Stores DIB dimensions */
// RECT        rcClip;
// RECT        rcPrint;
// unsigned	SelectImageSC;
// unsigned	SelectImageEC;
// unsigned	SelectImageSR;
// unsigned	SelectImageER;

char        str[64];            /* For message box */
// BOOL        mainwindow;         /* TRUE if main window being created */ 
// BOOL		previewup = FALSE;	/* TRUE if print preview is up */    
// BOOL		setupshown = FALSE;	/* TRUE if the print setup dalog was shown */
// DWORD       FlagSave;           /* Saved state of printer flags  */
// char		drives[32];			/* Drives */ 

/* Other files */
// char		NextSheet[64];
// char		PrevSheet[64];
// char		NextPage[64];
// char		PrevPage[64];

/* variables needed for scrolling */
static BOOL bScrollFlag = FALSE;
static unsigned int LastImageStartRow;
static unsigned int LastImageEndRow;
static unsigned int LastImageStartColumn;
static unsigned int LastImageEndColumn;
static BOOL bSaveMap=FALSE;

extern PRINTDLG pd;                              /* Common print dialog structure */

/* Definitions */
#define INIFILE		"RenTiff.INI"                                                                   
#define SKIPLIMIT	10
                                                                   

/* Prototypes */
void DisplayPage( HWND );
VOID FAR PASCAL ReadFile( HWND );
BOOL IncrementMap( char *, char * );
BOOL FillDirListBox( HWND, char * );

// VOID PASCAL GetImageSize( int );
// void SetScrollRanges( HWND );
// void ResetScrollBars( int, HWND );
// void PASCAL NormalizeRect( RECT * );
// void TrackMouse( HWND, POINT );
// void TrackMouseLB( HWND, POINT );
// void DrawSelect( HDC, BOOL );
// void DrawSelectFill( HDC, BOOL );
// void SetMenuOptions( void );

// BOOL FAR PASCAL PrintFile(HWND hWnd, int PImageHandle,int PrintMode, int Scaling);
// void ThePrintPreview( HWND, int, int,int);
// HDC GetPrinterDC(void);

/* Macro to restrict a given value to an upper or lower boundary value */
#define BOUND(x,min,max)    ((x) < (min) ? (min) : ((x) > (max) ? (max) : (x)))
#define SWAP(x,y)           ((x)^=(y)^=(x)^=(y))




/****************************************************************************

    FUNCTION: WinMain(HANDLE, HANDLE, LPSTR, int)

    PURPOSE: calls initialization function, processes message loop

****************************************************************************/

int PASCAL WinMain(
HINSTANCE hInstance,            /* Current instance */
HINSTANCE hPrevInstance,        /* Previous instance */
LPSTR lpCmdLine,                /* Command line */
int nCmdShow                    /* Show-window type (open/icon) */
)
{
    MSG     msg;                /* Message */

    if ( !hPrevInstance )       /* Other instances of app running? */
        if ( !InitApplication( hInstance ) ) /* Initialize shared things */
            return( FALSE );    /* Exits if unable to initialize     */

// 	/* Copy the file name */
// 	lstrcpy( (LPSTR) OpenName, (LPSTR) lpCmdLine );

    /* Perform initializations that apply to a specific instance */
    if ( !InitInstance( hInstance, nCmdShow ) )
        return( FALSE );

// 	/* Load accelerator table */
// 	hAccel = LoadAccelerators( hInstance, "MdiAccel" );
                                    
    /* Initialize annotations stuff; if it fails, disable annotations */
//  memset( &pd, 0x00, sizeof(PRINTDLG) );
//  pd.lStructSize  = sizeof(PRINTDLG);
//  pd.hwndOwner = hWndMain;
//  pd.hDevMode = NULL;
//  pd.hDevNames = NULL;
//  pd.Flags = PD_RETURNDEFAULT | PD_NOSELECTION | PD_NOPAGENUMS;
//  pd.nCopies = 1;
//  pd.hDC = GetPrinterDC( );
//  pd.Flags = PD_RETURNDC | PD_NOSELECTION | PD_NOPAGENUMS;

    /* Acquire and dispatch messages until a WM_QUIT message is received. */
    while( GetMessage( &msg, NULL, NULL, NULL) )
    {
//      if( !TranslateAccelerator( hWndMain, hAccel, &msg ) )
//      {
            TranslateMessage( &msg );   /* Translates virtual key codes */
            DispatchMessage( &msg );    /* Dispatches message to window */
//      }
    }

    /* End of message loop */  
    (void) FreeResource( hAccel );
    return( msg.wParam );               /* Terminate program */
}



/****************************************************************************

    FUNCTION: InitApplication(HANDLE)

    PURPOSE: Initializes window data and registers window class

****************************************************************************/

BOOL InitApplication(
HINSTANCE hInstance                 /* Current instance       */
)
{
    WNDCLASS    wc;                 /* Window class structure */

    /* Fill in window class structure with parameters */
    /* that describe the main window. */
    wc.style = NULL;                    /* Class style(s) */
    wc.lpfnWndProc = MainWndProc;       /* Message function */
    wc.cbClsExtra = 0;                  /* No per-class extra data */
    wc.cbWndExtra = 0;                  /* No per-window extra data */
    wc.hInstance = hInstance;           /* Application that owns the class */
    wc.hIcon = LoadIcon( hInstance, "rentiff" );
    wc.hCursor = LoadCursor( NULL, IDC_ARROW );
    wc.hbrBackground = (HBRUSH)GetStockObject( WHITE_BRUSH );
    wc.lpszMenuName =  "GenericMenu";   /* Name of menu resource */
    wc.lpszClassName = "GenericWClass"; /* Window class name */

    /* Register the window class and return success/failure code. */
    return( RegisterClass( &wc ) );
}



/****************************************************************************

    FUNCTION:  InitInstance(HANDLE, int)

    PURPOSE:  Saves instance handle and creates main window

****************************************************************************/

BOOL InitInstance( HINSTANCE hInstance, int nCmdShow )
{
	HWND		hWnd;               /* Main window handle */
	
	int			xScreen;			/* Screen width */
	int			yScreen;			/* Screen height */
	// int			xStart;				/* Left side of window */
	// int			yStart;				/* Top of window */
	// int			xEnd;				/* Right side of window */
	// int			yEnd; 				/* Bottom of window */
	
	// char		output[64];			/* Work variable */
	// char		szDefRight[10];		/* Default right side of window */
	// char		szDefBottom[10];	/* Default bottom of window */
	
	/* Get screen size and default window area */
    xScreen = GetSystemMetrics(SM_CXSCREEN);
    yScreen = GetSystemMetrics(SM_CYSCREEN);
    // xStart = 0;
    // yStart = 0;
    // yScreen -= 64;
    // xScreen -= 64;          
    
    // wsprintf( szDefRight, "%d", xScreen );
    // wsprintf( szDefBottom, "%d", yScreen );

	/* Retrieve last window size */                     
	/*
	GetPrivateProfileString( "CDMAPS", "Left", "0", output, 20, INIFILE );
	xStart = atoi(output);
	GetPrivateProfileString( "CDMAPS", "Top", "0", output, 20, INIFILE );
	yStart = atoi(output);
	GetPrivateProfileString( "CDMAPS", "Right", szDefRight, output, 20, INIFILE );
	xEnd = atoi(output);
	GetPrivateProfileString( "CDMAPS", "Bottom", szDefBottom, output, 20, INIFILE );
	yEnd = atoi(output);            
	*/
	
    // Preparing output directory
   char szIniFile[_MAX_PATH];
   getcwd(szIniFile, _MAX_PATH);
   strcat(szIniFile, "\\RenTiff.ini");
	GetPrivateProfileString( "SYSTEM", "SaveDir", "..\\", SaveImgDir, 255, szIniFile );
   if (_access(SaveImgDir, 0))
      CreateDirectory(SaveImgDir, NULL);
   sprintf(szIniFile, "%s\\BadMaps", SaveImgDir);
   if (_access(szIniFile, 0))
      CreateDirectory(szIniFile, NULL);
   sprintf(szIniFile, "%s\\NeedRotate", SaveImgDir);
   if (_access(szIniFile, 0))
      CreateDirectory(szIniFile, NULL);

    /* Save the instance handle in static variable, which will be used in  */
    /* many subsequence calls from this application to Windows.            */
    hInst = hInstance;

    /* Create a main window for this application instance.  */
    // mainwindow = TRUE;
    hWnd = CreateWindow(
        "GenericWClass",                /* See RegisterClass() call */
        "Image Renaming Program",  		/* Text for window title bar */
        WS_OVERLAPPEDWINDOW,            /* Window style */
        0,
        0,
        xScreen,
        yScreen,
        NULL,                           /* Overlapped windows have no parent */
        NULL,                           /* Use the window class menu */
        hInstance,                      /* This instance owns this window */
        NULL                            /* Pointer not needed */
    );

    /* If window could not be created, return "failure" */
    if ( !hWnd )
        return( FALSE );

    /* Make the window visible; update its client area; and return "success" */
    hWndMain = hWnd;
    ShowWindow( hWnd, nCmdShow );       /* Show the window */
    UpdateWindow( hWnd );               /* Sends WM_PAINT message */
    return( TRUE );                     /* Success */
}


/****************************************************************************

    FUNCTION: MainWndProc(HWND, UINT, WPARAM, LPARAM)

    PURPOSE:  Processes messages

    MESSAGES:

    WM_COMMAND    - application menu (About dialog box)
    WM_DESTROY    - destroy windows
    WM_CLOSE      - close windows  
    WM_SIZE       - resize and re-display windows
    WM_PAINT      - redisplay image in main window
    WM_RBUTTONDOWN- start mouse tracking
    WM_LBTTONDOWN - restore original image
    WM_VSCROLL    - scroll image up or down
    WM_HSCROLL    - scroll image left or right
    WM_COMMAND    - menu selection executed

 
****************************************************************************/

long CALLBACK _export MainWndProc(
HWND    hWnd,                    /* Window handle */
UINT    message,                 /* Type of message */
WPARAM  wParam,                  /* Additional information */
LPARAM  lParam                   /* Additional information */
)
{
    static HDC              hDC;        /* Display context */
    static PAINTSTRUCT      ps;         /* Paintstruct information */
    // static int              iMax;       /* Scroll bar data */
    // static int              iMin;       /* Scroll bar data */
    // static int              iPos;       /* Scroll bar data */
    // static int              dn;         /* Scroll bar data */
    static RECT             rc;         /* Size of current window */


    switch( message )
    {
    case WM_CREATE:
        // if ( mainwindow == FALSE )
        //     break;
        // hMenu = GetMenu( hWnd );
		// SetMenuOptions( );			/* Browse menu */
        // if ( OpenName[0] != 0 )
	    //     DisplayPage( hWnd );	/* Display the page */
        break;

    case WM_DESTROY:          /* message: window being destroyed */
        if ( ImageHandle >= 0 )
        {
            DecompClose( ImageHandle );
            ImageHandle = -1;
        }
        PostQuitMessage( 0 );
        break;

    case WM_CLOSE:
    	/*
        if ( hWnd == hWndMain )
        {
            RECT		XRect;
            static char output[64];

            GetWindowRect( hWnd, &XRect );
            wsprintf( output, "%d", XRect.left );
            WritePrivateProfileString( "CDMAPS", "Left", output, INIFILE  );
            wsprintf( output, "%d", XRect.top );
            WritePrivateProfileString( "CDMAPS", "Top", output, INIFILE );
            wsprintf( output, "%d", XRect.right );
            WritePrivateProfileString( "CDMAPS", "Right", output, INIFILE );
            wsprintf( output, "%d", XRect.bottom );
            WritePrivateProfileString( "CDMAPS", "Bottom", output, INIFILE );
            DeleteDC( pd.hDC );
       */
            DestroyWindow( hWnd );
        /*
            if ( ImageHandle >= 0 )
            {
                DecompClose( ImageHandle );
                ImageHandle = -1;
            }   
        } 
        */
        break;
                    
    case WM_PAINT:                         
//    	if ( ( previewup == TRUE ) && ( setupshown == TRUE ) )
//    		break;
        hDC = BeginPaint( hWnd, &ps );
//        if ( OpenName[0] == 0 )
//            EnableMenuItem( hMenu, IDM_PRINT, MF_GRAYED | MF_BYCOMMAND );
//        else
//            EnableMenuItem( hMenu, IDM_PRINT, MF_ENABLED | MF_BYCOMMAND ); 

/*
		if ( previewup == TRUE )
		{
			ThePrintPreview( hWndMain,ImageHandle,PrintSelection,PrintScaling);
		}            
        else 
*/
		if ( ImageHandle >= 0 )
        {
            // struct CurrentView View[1];
            DecompUpdateView( ImageHandle, FORCEREPAINT );
            // DecompGetView( ImageHandle, (struct CurrentView far *)View );
            // is there an inverted rect on screen?
            /*
            if ( (rcPrint.top != 0) ||
                 (rcPrint.bottom != 0) ||
                 (rcPrint.left != 0)   ||
                 (rcPrint.right != 0)  )
            {  // is the view the same?
               if((SelectImageSC == View->StartColumn) &&
                  (SelectImageEC == View->EndColumn) &&
                  (SelectImageSR == View ->StartRow) &&
                  (SelectImageER == View->EndRow)         )
               {
                  PatBlt( hDC, rcPrint.left, rcPrint.top,
                      rcPrint.right - rcPrint.left,
                      rcPrint.bottom - rcPrint.top,DSTINVERT);
               }
               else
               {
                  SelectImageSC = SelectImageEC = 0;
                  SelectImageSR = SelectImageER = 0;
               }
            }
            */
        }
        EndPaint( hWnd, &ps );
        break;

/*
    case WM_RBUTTONDOWN:
//        /* Start rubberbanding a rect. and track it's dimensions.
//        /* set the clip rectangle to it's dimensions.
        TrackMouse( hWnd, MAKEPOINT(lParam) );
        if ( ( rcClip.bottom - rcClip.top > 40 ) &&
            ( rcClip.right - rcClip.left > 40 ) )
        {
            DecompZoomRect( ImageHandle, rcClip.left, rcClip.top,
                rcClip.right, rcClip.bottom );
            ResetScrollBars( ImageHandle, hWnd );
        }
        InvalidateRect( hWnd, NULL, FALSE );
        rcClip.bottom = rcClip.top = 0;
        rcClip.left = rcClip.right = 0;
        break;

    case WM_MOUSEMOVE:
        break;       

    case WM_LBUTTONDOWN:            // /* Left mouse button released
//    	/* If rectangle already selected, clear it
    	if ( rcPrint.bottom != 0 )
    	{
           SelectImageSC = SelectImageEC = 0;
           SelectImageSR = SelectImageER = 0;
           rcPrint.bottom = 0;
           rcPrint.right = 0;
           rcPrint.top = 0;
           rcPrint.left = 0;
           // DecompViewRect( ImageHandle, FITHORZ );
           // ResetScrollBars( ImageHandle, hWnd );
           InvalidateRect( hWnd, NULL, FALSE );
    	}
    	
        TrackMouseLB( hWnd, MAKEPOINT(lParam) );
        if ( ( rcClip.bottom - rcClip.top < 32 ) &&
            ( rcClip.right - rcClip.left < 32 ) )
        {  // code to allow just a click of the left mouse button to reset
           // the screen, do not know if this is desired.
           SelectImageSC = SelectImageEC = 0;
           SelectImageSR = SelectImageER = 0;
           rcPrint.bottom = 0;
           rcPrint.right = 0;
           rcPrint.top = 0;
           rcPrint.left = 0;
           DecompViewRect( ImageHandle, FITHORZ );
           ResetScrollBars( ImageHandle, hWnd );
           InvalidateRect( hWnd, NULL, FALSE );
        }
        else
        {  // save clipping values
           struct CurrentView View[1];
           DecompGetView(ImageHandle,(struct CurrentView far *)View);
           SelectImageSC = View->StartColumn;
           SelectImageEC = View->EndColumn;
           SelectImageSR = View->StartRow;
           SelectImageER = View->EndRow;
           rcPrint.bottom = rcClip.bottom;
           rcPrint.right = rcClip.right;
           rcPrint.top = rcClip.top;
           rcPrint.left = rcClip.left;
           rcClip.bottom = rcClip.top = 0;
           rcClip.left = rcClip.right = 0;
        }
        rcClip.bottom = rcClip.top = 0;
        rcClip.left = rcClip.right = 0;
        break;
*/

    case WM_SIZE:
        /* On creation or resize, size the edit control. */
        if ( ( hWnd == hWndMain ) && ( ImageHandle >= 0 ) )
        {
            GetClientRect( hWnd, &rc );
            DecompSetViewRect( ImageHandle, 0, 0, rc.right, rc.bottom );
            /*
            GetImageSize( ImageHandle );
            SetScrollRanges( hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            */
        }
        break;

/*
        case WM_VSCROLL:
//            /* Calculate new vertical scroll position
            GetScrollRange (hWnd, SB_VERT, &iMin, &iMax);
            iPos = GetScrollPos (hWnd, SB_VERT);
            GetClientRect (hWnd, &rc);

            switch (wParam) {
                case SB_LINEDOWN:      dn =  rc.bottom / 16 + 1;
                                       break;

                case SB_LINEUP:        dn = -rc.bottom / 16 + 1;
                                       break;

                case SB_PAGEDOWN:      dn =  rc.bottom / 2  + 1;
                                       break;

                case SB_PAGEUP:        dn = -rc.bottom / 2  + 1;
                                       break;

                case SB_THUMBTRACK:
                case SB_THUMBPOSITION: dn = LOWORD(lParam)-iPos;
                                       break;

                default:               dn = 0;
            }
//            /* Limit scrolling to current scroll range
            if ((dn = BOUND (iPos + dn, iMin, iMax) - iPos) &&
                (wParam != 8))
            {
                static                   int top,bottom,left,right;
                static                   int sc,ec,sr,er;
                static                   int ImageHandleTemp;
                static                   struct CurrentView View[1],Viewx[1];
                static                   RECT ps;
                rcPrint.bottom = 0;
                rcPrint.right = 0;
                rcPrint.top = 0;
                rcPrint.left = 0;

                bScrollFlag = TRUE;
                ScrollWindowEx(hWnd,0,-dn,NULL,NULL,NULL,&ps,SW_ERASE);

                DecompGetView(ImageHandle,Viewx);
                LastImageStartColumn = Viewx->StartColumn;
                LastImageStartRow = Viewx->StartRow;
                LastImageEndColumn = Viewx->EndColumn;
                LastImageEndRow = Viewx->EndRow;
                DecompScroll(ImageHandle,VERTSCALED,iPos + dn);
                SetScrollPos (hWnd, SB_VERT, iPos + dn, TRUE);

                ImageHandleTemp = DecompCopyImage(ImageHandle);

                DecompGetView(ImageHandleTemp,View);
                top = ps.top;
                bottom = ps.bottom;
                left = ps.left;
                right = ps.right;


                sr = View->StartRow;
                er = View->EndRow;
                sc = View->StartColumn;
                ec = View->EndColumn;
                if(View->Rotation == ROTATION_0)
                {
                   if(LastImageStartRow < View->StartRow)
                   {  // scroll down
                      if(View->StartRow > LastImageEndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = LastImageEndRow;
                         er = View->EndRow;
                      }
                   }
                   else
                   {  // scroll up
                      sr = View->StartRow;
                      er = LastImageStartRow;
                   }
                }
                if(View->Rotation == ROTATION_180)
                {
                   if(LastImageStartRow < View->StartRow)
                   {  // scroll up
                      if(View->StartRow > LastImageEndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = LastImageEndRow;
                         er = View->EndRow;
                      }
                   }
                   else
                   {  // scroll down
                      if(LastImageStartRow > View->EndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = View->StartRow;
                         er = LastImageStartRow;
                      }
                   }
                }
                if(View->Rotation == ROTATION_90)
                {
                   if(LastImageStartColumn < View->StartColumn)
                   {  // scroll right
                      if(View->StartColumn > LastImageEndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = LastImageEndColumn;
                         ec = View->EndColumn;
                      }
                   }
                   else
                   {  // scroll left
                      sc = View->StartColumn;
                      ec = LastImageStartColumn;
                   }
                }
                if(View->Rotation == ROTATION_270)
                {
                   if(LastImageStartColumn < View->StartColumn)
                   {  // scroll up
                      if(View->StartColumn > LastImageEndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = LastImageEndColumn;
                         ec = View->EndColumn;
                      }
                   }
                   else
                   {  // scroll down
                      if(LastImageStartColumn > View->EndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = View->StartColumn;
                         ec = LastImageStartColumn;
                      }
                   }
                }
                DecompSetViewRect(ImageHandleTemp,0,top,
                                     right - left,
                                     bottom);
                DecompRectNoScaling(ImageHandleTemp,sc,
                                             sr,
                                             ec,
                                             er);
                DecompVertScale(ImageHandleTemp,Viewx->ScaleVert,
                                                Viewx->ScaleVertTenths,
                                                Viewx->ScaleVertExpand);
                DecompHorzScale(ImageHandleTemp,Viewx->ScaleHorz,
                                                Viewx->ScaleHorzTenths,
                                                Viewx->ScaleHorzExpand);

                DecompOutputGreyScale(ImageHandleTemp,OUTPUTWINDOWSOFFG,
                             hWnd,ImageHandleTemp,0);
                DecompOutput(ImageHandleTemp,OUTPUTWINDOWSOFFM,
                             hWnd,ImageHandleTemp,0);

                DecompUpdateView(ImageHandleTemp,FORCEREPAINT);
                DecompClose(ImageHandleTemp);
            }
            break;

        case WM_HSCROLL:
//            /* Calculate new horizontal scroll position
            GetScrollRange (hWnd, SB_HORZ, &iMin, &iMax);
            iPos = GetScrollPos (hWnd, SB_HORZ);
            GetClientRect (hWnd, &rc);

            switch (wParam) {
                case SB_LINEDOWN:      dn =  rc.right / 16 + 1;
                                       break;

                case SB_LINEUP:        dn = -rc.right / 16 + 1;
                                       break;

                case SB_PAGEDOWN:      dn =  rc.right / 2  + 1;
                                       break;

                case SB_PAGEUP:        dn = -rc.right / 2  + 1;
                                       break;

                case SB_THUMBTRACK:
                case SB_THUMBPOSITION: dn = LOWORD (lParam) - iPos;
                                       break;

                default:               dn = 0;
            }
//            /* Limit scrolling to current scroll range 
            if ((dn = BOUND (iPos + dn, iMin, iMax) - iPos) &&
                (wParam != 8))
            {
                static                   int top,bottom,left,right;
                static                   int sc,ec,sr,er;
                static                   int ImageHandleTemp;
                static                   struct CurrentView View[1],Viewx[1];
                static                   RECT ps;
                rcPrint.bottom = 0;
                rcPrint.right = 0;
                rcPrint.top = 0;
                rcPrint.left = 0;

                ScrollWindowEx(hWnd,-dn,0,NULL,NULL,NULL,&ps,SW_ERASE);

                DecompGetView(ImageHandle,Viewx);
                LastImageStartColumn = Viewx->StartColumn;
                LastImageStartRow = Viewx->StartRow;
                LastImageEndColumn = Viewx->EndColumn;
                LastImageEndRow = Viewx->EndRow;
                DecompScroll(ImageHandle,HORZSCALED,iPos + dn);
                SetScrollPos (hWnd, SB_HORZ, iPos + dn, TRUE);

                ImageHandleTemp = DecompCopyImage(ImageHandle);

                DecompGetView(ImageHandleTemp,View);
                top = ps.top;
                bottom = ps.bottom;
                left = ps.left;
                right = ps.right;

                sr = View->StartRow;
                er = View->EndRow;
                sc = View->StartColumn;
                ec = View->EndColumn;
                if(View->Rotation == ROTATION_0)
                {
                   if(LastImageStartColumn < View->StartColumn)
                   {  // scroll right
                      if(View->StartColumn > LastImageEndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = LastImageEndColumn;
                         ec = View->EndColumn;
                      }
                   }
                   else
                   {  // scroll left
                      sc = View->StartColumn;
                      ec = LastImageStartColumn;
                   }
                }
                if(View->Rotation == ROTATION_180)
                {
                   if(LastImageStartColumn < View->StartColumn)
                   {  // scroll up
                      if(View->StartColumn > LastImageEndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = LastImageEndColumn;
                         ec = View->EndColumn;
                      }
                   }
                   else
                   {  // scroll down
                      if(LastImageStartColumn > View->EndColumn)
                      {
                         sc = View->StartColumn;
                         ec = View->EndColumn;
                      }
                      else
                      {
                         sc = View->StartColumn;
                         ec = LastImageStartColumn;
                      }
                   }
                }
                if(View->Rotation == ROTATION_90)
                {
                   if(LastImageStartRow < View->StartRow)
                   {  // scroll up
                      if(View->StartRow > LastImageEndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = LastImageEndRow;
                         er = View->EndRow;
                      }
                   }
                   else
                   {  // scroll down
                      if(LastImageStartRow > View->EndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = View->StartRow;
                         er = LastImageStartRow;
                      }
                   }
                }
                if(View->Rotation == ROTATION_270)
                {
                   if(LastImageStartRow < View->StartRow)
                   {  // scroll down
                      if(View->StartRow > LastImageEndRow)
                      {
                         sr = View->StartRow;
                         er = View->EndRow;
                      }
                      else
                      {
                         sr = LastImageEndRow;
                         er = View->EndRow;
                      }
                   }
                   else
                   {  // scroll up
                      sr = View->StartRow;
                      er = LastImageStartRow;
                   }
                }

                DecompSetViewRect(ImageHandleTemp,left,0,
                                     right,
                                     bottom - top);
                DecompRectNoScaling(ImageHandleTemp,sc,
                                             sr,
                                             ec,
                                             er);
                DecompVertScale(ImageHandleTemp,Viewx->ScaleVert,
                                                Viewx->ScaleVertTenths,
                                                Viewx->ScaleVertExpand);
                DecompHorzScale(ImageHandleTemp,Viewx->ScaleHorz,
                                                Viewx->ScaleHorzTenths,
                                                Viewx->ScaleHorzExpand);

                DecompOutputGreyScale(ImageHandleTemp,OUTPUTWINDOWSOFFG,
                             hWnd,ImageHandleTemp,0);
                DecompOutput(ImageHandleTemp,OUTPUTWINDOWSOFFM,
                             hWnd,ImageHandleTemp,0);

                DecompUpdateView(ImageHandleTemp,FORCEREPAINT);
                DecompClose(ImageHandleTemp);
            }
            break;
*/

    case WM_COMMAND:
        switch( LOWORD(wParam) )
        {
        /*
        case IDM_UP:
            SendMessage( hWnd, WM_VSCROLL, SB_LINEUP, 0L );
            break;

        case IDM_DOWN:
            SendMessage( hWnd, WM_VSCROLL, SB_LINEDOWN, 0L );
            break;

        case IDM_LEFT:
            SendMessage( hWnd, WM_HSCROLL, SB_LINEUP, 0L );
            break;

        case IDM_RIGHT:
            SendMessage( hWnd, WM_HSCROLL, SB_LINEDOWN, 0L );
            break;
        */
        
        case IDM_EXIT:
            if ( ImageHandle >= 0 )
            {
                DecompClose( ImageHandle );
                ImageHandle = -1;
            }
            SendMessage( hWnd, WM_CLOSE, 0, 0L );
//            DestroyWindow( hWnd );
//            return( 0 );
            break;

        case IDM_ABOUT:
            DialogBox( hInst, "AboutBox", hWnd, About );
            break;                
            
        case IDM_PARAMETERS:
			DialogBox( hInst, "Parameters", hWnd, Parameters );
        	break;
        
        /*
        case IDM_PRINT:

            if ( ImageHandle >= 0 )
            { 
                        int rv;
                // do print dialog
                // need to put to desktop so we can hide the dialog box   
                setupshown = FALSE;
                rv = DialogBox( hInst, "AdvPrintBox", hWnd, AdvPrint );
                if(rv == TRUE)
                {                           
				   SetCursor(LoadCursor(NULL, IDC_WAIT));
                   PrintFile( hwnd,ImageHandle, PrintSelection, PrintScaling );
                   SetCursor(LoadCursor(NULL,IDC_ARROW));
                   // remove the print rectangle setting ???
                   rcPrint.bottom = 0;
                   rcPrint.right = 0;
                   rcPrint.top = 0;
                   rcPrint.left = 0;
               } 
               else if(rv == 2)
               {                                       
					previewup = TRUE;
                  ThePrintPreview( hWndMain,ImageHandle,PrintSelection,PrintScaling);
                  rv = DialogBox( hInst, "CloseBox", hWndMain, CloseBox );
					previewup = FALSE;
               		if(rv == 2)
                	{                           
				   	SetCursor(LoadCursor(NULL, IDC_WAIT));
                   	PrintFile( hwnd,ImageHandle, PrintSelection, PrintScaling );
                   	SetCursor(LoadCursor(NULL,IDC_ARROW));
                   	// remove the print rectangle setting ???
                   	rcPrint.bottom = 0;
                   	rcPrint.right = 0;
                   	rcPrint.top = 0;
                   	rcPrint.left = 0;  
                   	}
               }                   
               setupshown = FALSE;
               InvalidateRect( hWnd, NULL, FALSE );
            }
            break;
		*/
		
        case IDM_FILE:
            ReadFile( hWnd );
			// SetMenuOptions( );			/* Browse menu */
//			if ( OpenName[0] != 0 )
	            DialogBox( hInst, "Rename", hWnd, RenameFile );
//                DisplayPage( hWnd );
            break;

		/*
        case IDM_ZOOMIN:
            DecompZoom( ImageHandle, ZOOMIN );
            ResetScrollBars( ImageHandle, hWnd );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ZOOMOUT:
            DecompZoom( ImageHandle, ZOOMOUT );
            ResetScrollBars( ImageHandle, hWnd );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWFITHORZ:
            DecompViewRect( ImageHandle, FITHORZ );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWFITVERT:
            DecompViewRect( ImageHandle, FITVERT );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWICON:
            DecompViewRect( ImageHandle, FITICON );
            ResetScrollBars( ImageHandle, hWnd );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEW1TO1:
            DecompHorzScale( ImageHandle, 1, 0, 0 );
            DecompVertScale( ImageHandle, 1, 0, 0 );
            ResetScrollBars( ImageHandle, hWnd );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWDARK:
            DecompContrast( ImageHandle, DARKER );
            InvalidateRect( hWnd, NULL, FALSE );
            break;
        case IDM_VIEWGRAY:
            DecompScaling(ImageHandle,SCALETOGREY);
            InvalidateRect( hWnd, NULL, FALSE );
            break;
        case IDM_VIEWMONO:
            DecompScaling(ImageHandle,SCALETOBINARY);
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWLIGHT:
            DecompContrast( ImageHandle, LIGHTER );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWREVERSE:
            DecompReverse( ImageHandle, 0 );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWBACKBLACK:
            DecompEOLFill( ImageHandle, BLACK );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_VIEWBACKWHITE:
            DecompEOLFill( ImageHandle, WHITE );
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROTRIGHT:
            DecompRotation( ImageHandle, ROTATE_RIGHT );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROTLEFT:
            DecompRotation( ImageHandle, ROTATE_LEFT );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROT0:
            DecompRotation( ImageHandle, ROTATION_0 );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROT90:
            DecompRotation( ImageHandle, ROTATION_90 );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROT180:
            DecompRotation( ImageHandle, ROTATION_180 );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;

        case IDM_ROT270:
            DecompRotation( ImageHandle, ROTATION_270 );
            ResetScrollBars( ImageHandle, hWnd );
            rcPrint.bottom = 0;
            rcPrint.right = 0;
            rcPrint.top = 0;
            rcPrint.left = 0;
            InvalidateRect( hWnd, NULL, FALSE );
            break;           
            
        case IDM_FULLMAP:
			SelectImageSC = SelectImageEC = 0;
			SelectImageSR = SelectImageER = 0;
			rcPrint.bottom = 0;
			rcPrint.right = 0;
			rcPrint.top = 0;
			rcPrint.left = 0;
			DecompViewRect( ImageHandle, FITHORZ );
			ResetScrollBars( ImageHandle, hWnd );
			InvalidateRect( hWnd, NULL, FALSE );
        	break;                        
        	
        case IDM_NEXTSHEET:
        	strcpy( OpenName, NextSheet );
        	SetMenuOptions( );     
        	DisplayPage( hWnd );
        	break;

        case IDM_PREVSHEET:
        	strcpy( OpenName, PrevSheet );
        	SetMenuOptions( );     
        	DisplayPage( hWnd );
        	break;

        case IDM_NEXTPAGE:
        	strcpy( OpenName, NextPage );
        	SetMenuOptions( );     
        	DisplayPage( hWnd );
        	break;

        case IDM_PREVPAGE:
        	strcpy( OpenName, PrevPage );
        	SetMenuOptions( );     
        	DisplayPage( hWnd );
        	break;

        case IDM_COPY:
            break;
        */
        }
        break;

    default:                  /* Passes it on if unprocessed */
        return( DefWindowProc( hWnd, message, wParam, lParam ) );
    }  /* end switch main */

    return( NULL );
}



/****************************************************************************

    FUNCTION:   About(HWND, unsigned, WPARAM, LPARAM)

    PURPOSE:    Processes messages for "About" dialog box

    MESSAGES:

    WM_INITDIALOG - initialize dialog box
    WM_COMMAND    - Input received

****************************************************************************/

BOOL _export CALLBACK About(
HWND        hDlg,           /* Window handle of the dialog box */
unsigned    message,        /* Type of message                 */
WPARAM      wParam,         /* Message-specific information    */
LPARAM      lParam
)
{
    switch( message )
    {
    case WM_INITDIALOG:     /* Message: initialize dialog box */
		return( TRUE );

    case WM_COMMAND:        /* Message: received a command */
        if ( LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL )
        {
            EndDialog( hDlg, TRUE );
            return( TRUE );
        }
        break;
    }

    return( FALSE );        /* Didn't process a message */
}         



/****************************************************************************

    FUNCTION:   Parameters(HWND, unsigned, WPARAM, LPARAM)

    PURPOSE:    Processes messages for "Parameters" dialog box

    MESSAGES:

    WM_INITDIALOG - initialize dialog box
    WM_COMMAND    - Input received

****************************************************************************/

BOOL _export CALLBACK Parameters(
HWND        hDlg,           /* Window handle of the dialog box */
unsigned    message,        /* Type of message                 */
WPARAM      wParam,         /* Message-specific information    */
LPARAM      lParam
) 
{
    switch( message )
    {
    case WM_INITDIALOG:     /* Message: initialize dialog box */ 
    	if ( nDisplay == DIS_FULL_MAP )
	        CheckDlgButton( hDlg, IDC_DISPLAY1, TRUE );
	    else if ( nDisplay == DIS_UPPER_LEFT )
	        CheckDlgButton( hDlg, IDC_DISPLAY2, TRUE );
	    else if ( nDisplay == DIS_UPPER_RIGHT )
	        CheckDlgButton( hDlg, IDC_DISPLAY3, TRUE );
	    else if ( nDisplay == DIS_LOWER_LEFT )
	        CheckDlgButton( hDlg, IDC_DISPLAY4, TRUE );
	    else if ( nDisplay == DIS_LOWER_RIGHT )
	        CheckDlgButton( hDlg, IDC_DISPLAY5, TRUE );
	        
    	if ( nRotate == ROT_UPRIGHT )
	        CheckDlgButton( hDlg, IDC_ROTATION1, TRUE );
    	else if ( nRotate == ROT_RIGHT )
	        CheckDlgButton( hDlg, IDC_ROTATION2, TRUE );
    	else if ( nRotate == ROT_LEFT )
	        CheckDlgButton( hDlg, IDC_ROTATION3, TRUE );
    	else if ( nRotate == ROT_UPSIDEDOWN )
	        CheckDlgButton( hDlg, IDC_ROTATION4, TRUE );

    	if ( nDocType == TYP_MAPS )
	        CheckDlgButton( hDlg, IDC_DOCTYPE1, TRUE );
    	else if ( nDocType == TYP_RECORDER )
	        CheckDlgButton( hDlg, IDC_DOCTYPE2, TRUE );    

		CheckDlgButton( hDlg, ID_AUTONUM, bAutoNum );
        SetDlgItemText( hDlg, ID_FILEMASK, FileExt );
		SendDlgItemMessage( hDlg, ID_FILEMASK, EM_LIMITTEXT, 10, 0L );
        return( TRUE );

    case WM_COMMAND:        /* Message: received a command */
        if ( LOWORD(wParam) == IDOK )
        {
			if ( IsDlgButtonChecked( hDlg, IDC_DISPLAY1 ) )
				nDisplay = DIS_FULL_MAP;
			else if ( IsDlgButtonChecked( hDlg, IDC_DISPLAY2 ) )
				nDisplay = DIS_UPPER_LEFT;
			else if ( IsDlgButtonChecked( hDlg, IDC_DISPLAY3 ) )
				nDisplay = DIS_UPPER_RIGHT;
			else if ( IsDlgButtonChecked( hDlg, IDC_DISPLAY4 ) )
				nDisplay = DIS_LOWER_LEFT;
			else if ( IsDlgButtonChecked( hDlg, IDC_DISPLAY5 ) )
				nDisplay = DIS_LOWER_RIGHT;
				
			if ( IsDlgButtonChecked( hDlg, IDC_ROTATION1 ) )
				nRotate = ROT_UPRIGHT;
			else if ( IsDlgButtonChecked( hDlg, IDC_ROTATION2 ) )
				nRotate = ROT_RIGHT;
			else if ( IsDlgButtonChecked( hDlg, IDC_ROTATION3 ) )
				nRotate = ROT_LEFT;
			else if ( IsDlgButtonChecked( hDlg, IDC_ROTATION4 ) )
				nRotate = ROT_UPSIDEDOWN;
				
			if ( IsDlgButtonChecked( hDlg, IDC_DOCTYPE1 ) )
				nDocType = TYP_MAPS;
			else if ( IsDlgButtonChecked( hDlg, IDC_DOCTYPE2 ) )
				nDocType = TYP_RECORDER;
				
			bAutoNum = IsDlgButtonChecked( hDlg, ID_AUTONUM );
			GetDlgItemText( hDlg, ID_FILEMASK, FileExt, sizeof(FileExt) - 1 );
            EndDialog( hDlg, TRUE );
            return( TRUE );
        }
        else if ( LOWORD(wParam) == IDCANCEL )
        {
            EndDialog( hDlg, FALSE );
            return( FALSE );
        }
        break;
    }

    return( FALSE );        /* Didn't process a message */
}         


/****************************************************************************

    FUNCTION:   CloseBox(HWND, unsigned, WORD, LONG)

    PURPOSE:    Processes messages for "CloseBox" dialog box

    MESSAGES:

    WM_INITDIALOG - initialize dialog box
    WM_COMMAND    - Input received

****************************************************************************/

/*
BOOL _export CALLBACK CloseBox( hDlg, message, wParam, lParam )
HWND        hDlg;           
unsigned    message;        
WORD        wParam;         
LONG        lParam;
{
	switch( message )
	{
	case WM_INITDIALOG:     
		return( TRUE );

	case WM_COMMAND:        
		if ( wParam == IDCANCEL )
		{
			EndDialog( hDlg, TRUE );
			return( TRUE );
		}
		else if ( wParam == IDOK )
		{
			EndDialog( hDlg, 2 );
			return( TRUE );
		}
		break;
	}

	return( FALSE );        
}   
*/


/****************************************************************************

    FUNCTION:   AdvPrint(HWND, unsigned, WORD, LONG)

    PURPOSE:    Processes messages for "AdvPrint" dialog box

    MESSAGES:

    WM_INITDIALOG - initialize dialog box
    WM_COMMAND    - Input received

****************************************************************************/
/*
BOOL _export CALLBACK AdvPrint( hDlg, message, wParam, lParam )
HWND        hDlg;
unsigned    message;
WORD        wParam;
LONG        lParam;
{
    switch( message )
    {
    case WM_INITDIALOG:     
        if ( ( rcPrint.bottom == 0 ) || ( rcPrint.right == 0 ) )
        {
            EnableWindow( GetDlgItem( hDlg, IDC_SELECTION ), FALSE );
        }
        if ( ( PrintSelection == TRUE ) && ( rcPrint.right != 0 ) )
        {
           CheckDlgButton( hDlg, IDC_ENTIRE, 0 );
           CheckDlgButton( hDlg, IDC_SELECTION, 1 );
        }
        else
        {
           CheckDlgButton( hDlg, IDC_ENTIRE, 1 );
           CheckDlgButton( hDlg, IDC_SELECTION, 0 );
        }
        if ( PrintScaling == FALSE )
        {
           CheckDlgButton( hDlg, IDC_SCALE, 1 );
           CheckDlgButton( hDlg, IDC_ORIGINAL, 0 );
        }
        else
        {
           CheckDlgButton( hDlg, IDC_SCALE, 0 );
           CheckDlgButton( hDlg, IDC_ORIGINAL, 1 );
        }
        return( TRUE );

    case WM_COMMAND:        
        if ( wParam == IDC_PRINTSETUP )
        {
            HDC hSaveDC;
            hSaveDC = pd.hDC;
            FlagSave = pd.Flags;
            pd.Flags |= PD_PRINTSETUP;
            PrintDlg((LPPRINTDLG)&pd);
            if ( pd.hDC != hSaveDC )
            {
               DeleteDC(hSaveDC);
            }
            pd.Flags = FlagSave;
            setupshown = TRUE;
//            EndDialog( hDlg, FALSE);
//            ShowWindow(hDlg,SW_SHOWNORMAL);
            return( TRUE );
        }      
        
        if ( wParam == IDCANCEL )
        {
            EndDialog( hDlg, FALSE );
            return( TRUE );
        }
        if (( wParam == IDOK) || (wParam == IDC_PRINTPREVIEW))
        {

            if(IsDlgButtonChecked(hDlg,IDC_ENTIRE))
               PrintSelection = FALSE;
            else
               PrintSelection = TRUE;

            if(IsDlgButtonChecked(hDlg,IDC_SCALE))
               PrintScaling = FALSE;
            else
               PrintScaling = TRUE;

            if ( wParam == IDC_PRINTPREVIEW )
               EndDialog( hDlg, 2 );
            else
               EndDialog( hDlg, TRUE );
            return( TRUE );
        }
        break;
    }

    return( FALSE );
}
*/


/***************************************************************************
                                                                          
  FUNCTION   : GetRealClientRect(HWND hwnd, LPRECT lprc)                  
                                                                          
  PURPOSE    : Calculates the client rectangle taking scrollbars into     
               consideration.                                             
                                                                         
****************************************************************************/ 
/*
void GetRealClientRect( hwnd, lprc )
HWND    hwnd;
PRECT   lprc;
{
    DWORD   dwStyle;

    dwStyle = GetWindowLong( hwnd, GWL_STYLE );
    GetClientRect( hwnd, lprc );

    if ( dwStyle & WS_HSCROLL )
        lprc->bottom += GetSystemMetrics( SM_CYHSCROLL );

    if ( dwStyle & WS_VSCROLL )
        lprc->right  += GetSystemMetrics( SM_CXVSCROLL );
}
*/


/***************************************************************************
                                                                          
  FUNCTION   : SetScrollRanges( hwnd )                                    
                                                                          
  PURPOSE    : Sets internal ranges of scroll bars for images                                                           
                                                                          
****************************************************************************/
/*
void SetScrollRanges( hwnd )
HWND    hwnd;
{
    RECT        rc;
    int         iRangeH, iRangeV;

    GetRealClientRect( hwnd, &rc );

    iRangeV = ptSize.y - rc.bottom;
    iRangeH = ptSize.x - rc.right;

    if ( iRangeH < 0 ) iRangeH = 0;
    if ( iRangeV < 0 ) iRangeV = 0;

    if ( GetScrollPos( hwnd, SB_VERT ) > iRangeV ||
        GetScrollPos( hwnd, SB_HORZ ) > iRangeH )
    {
        InvalidateRect( hwnd, NULL, FALSE );
    }

    SetScrollRange( hwnd, SB_VERT, 0, iRangeV, TRUE );
    SetScrollRange( hwnd, SB_HORZ, 0, iRangeH, TRUE );
}
*/
                           
                           
/***************************************************************************
                                                                          
  FUNCTION   : GetImageSize( hwnd )                                    
                                                                          
  PURPOSE    : Calculates dimensions of image at current scale                                                           
                                                                          
****************************************************************************/
VOID PASCAL GetImageSize( int handle )
{
    ptSize.x = DecompHorzScaledSize( handle );
    ptSize.y = DecompVertScaledSize( handle );
}


/***************************************************************************
                                                                          
  FUNCTION   : ResetScrollBars( hwnd )                                    
                                                                          
  PURPOSE    : Reset scroll bars after size change                                                           
                                                                          
****************************************************************************/
/*
void ResetScrollBars( int ImageHandle, HWND hwnd )
{
    int         VertPos;
    int         HorzPos;

    GetImageSize( ImageHandle );
    SetScrollRanges( hwnd );
    VertPos = DecompScrollGetVertPos( ImageHandle );
    HorzPos = DecompScrollGetHorzPos( ImageHandle );

    if ( HorzPos > ptSize.x )
        HorzPos = ptSize.x;
    if ( VertPos > ptSize.y )
        VertPos = ptSize.y;

    SetScrollPos( hwnd, SB_VERT, VertPos, TRUE );
    SetScrollPos( hwnd, SB_HORZ, HorzPos, TRUE );
}
*/


/***************************************************************************
                                                                          
  FUNCTION   : NormalizeRect(RECT *prc)                                   
                                                                          
  PURPOSE    : If the rectangle coordinates are reversed, swaps them      
                                                                          
***************************************************************************/
/*
void PASCAL NormalizeRect( prc )
RECT *prc;
{
    if ( prc->right < prc->left )
        SWAP( prc->right, prc->left );
    if ( prc->bottom < prc->top )
        SWAP( prc->bottom, prc->top );
}
*/


/**************************************************************************
                                                                          
  FUNCTION   : TrackMouse(HWND hwnd, POINT pt)                            
                                                                          
  PURPOSE    : Draws a rubberbanding rectangle and displays it's          
               dimensions till the mouse button is released               
                                                                          
***************************************************************************/  
/*
void TrackMouse( hwnd, pt )
HWND        hwnd;
POINT       pt;
{
    HDC     hdc;
    MSG     msg;
    POINT   ptOrigin;


//    /* Get the display context and trap the mouse
    hdc = GetDC( hwnd );
    SetCapture( hwnd );

//    /* Get mouse coordinates relative to origin of DIB 
    ptOrigin.x = 0;
    ptOrigin.y = 0;
    pt.x += 0;
    pt.y += 0;

//    /* Display the coordinates 
    SetWindowOrg( hdc, ptOrigin.x, ptOrigin.y );
    DrawSelect( hdc, FALSE );

//    /* Initialize clip rectangle to the point 
    rcClip.left   = pt.x;
    rcClip.top    = pt.y;
    rcClip.right  = pt.x;
    rcClip.bottom = pt.y;

//    /* Eat mouse messages until a WM_LBUTTONUP is encountered. 
//    /* Meanwhile continue to draw a rubberbanding rectangle
//    /* and display its dimensions 
    for ( ; ; )
    {
        if ( GetMessage( &msg, NULL, WM_MOUSEFIRST, WM_MOUSELAST ) )
        {
            DrawSelect( hdc, FALSE );
            rcClip.left   = pt.x;
            rcClip.top    = pt.y;
            rcClip.right  = LOWORD( msg.lParam ) + ptOrigin.x;
            rcClip.bottom = HIWORD( msg.lParam ) + ptOrigin.y;

            NormalizeRect( &rcClip );
            DrawSelect( hdc, TRUE );

            if ( msg.message == WM_RBUTTONUP )
                break;
        }
        else
            continue;
    }

    ReleaseCapture( );
    ReleaseDC( hwnd, hdc );
} 
*/

/**************************************************************************
                                                                          
  FUNCTION   : TrackMouseLB(HWND hwnd, POINT pt)
                                                                          
  PURPOSE    : Draws a filled rectangle
                                                                          
***************************************************************************/  
/*
void TrackMouseLB( hwnd, pt )
HWND        hwnd;
POINT       pt;
{
    HDC     hdc;
    MSG     msg;
    POINT   ptOrigin;


//    /* Get the display context and trap the mouse
    hdc = GetDC( hwnd );
    SetCapture( hwnd );

//    /* Get mouse coordinates relative to origin of DIB
    ptOrigin.x = 0;
    ptOrigin.y = 0;
    pt.x += 0;
    pt.y += 0;

//    /* Display the coordinates
    SetWindowOrg( hdc, ptOrigin.x, ptOrigin.y );
    DrawSelectFill( hdc, FALSE );

//    /* Initialize clip rectangle to the point 
    rcClip.left   = pt.x;
    rcClip.top    = pt.y;
    rcClip.right  = pt.x;
    rcClip.bottom = pt.y;

//    /* Eat mouse messages until a WM_LBUTTONUP is encountered. 
//    /* Meanwhile continue to draw a rubberbanding rectangle
//    /* and display its dimensions
    for ( ; ; )
    {
        if ( GetMessage( &msg, NULL, WM_MOUSEFIRST, WM_MOUSELAST ) )
        {
            DrawSelectFill( hdc, FALSE );
            rcClip.left   = pt.x;
            rcClip.top    = pt.y;
            rcClip.right  = LOWORD( msg.lParam ) + ptOrigin.x;
            rcClip.bottom = HIWORD( msg.lParam ) + ptOrigin.y;

            NormalizeRect( &rcClip );
            DrawSelectFill( hdc, TRUE );

            if ( msg.message == WM_LBUTTONUP )
                break;
        }
        else
            continue;
    }

    ReleaseCapture( );
    ReleaseDC( hwnd, hdc );
}
*/



/**************************************************************************
                                                                          
  FUNCTION   :  DrawSelect(HDC hdc, BOOL fDraw)                           
  
  PURPOSE    :  Draws the selected clip rectangle with its dimensions on  
                the DC/screen                                             
                                                                          
***************************************************************************/   

/*
void DrawSelect( hdc, fDraw )
HDC     hdc;
BOOL    fDraw;
{
    if ( !IsRectEmpty( &rcClip ) ) 
    {
//        /* If a rectangular clip region has been selected, draw it
        PatBlt( hdc, rcClip.left, rcClip.top, rcClip.right-rcClip.left, 
            1, DSTINVERT );
        PatBlt( hdc, rcClip.left, rcClip.bottom, 1, 
            -(rcClip.bottom-rcClip.top), DSTINVERT );
        PatBlt( hdc, rcClip.right-1, rcClip.top, 1, 
            rcClip.bottom-rcClip.top, DSTINVERT );
        PatBlt( hdc, rcClip.right, rcClip.bottom-1, 
            -(rcClip.right-rcClip.left), 1, DSTINVERT );
    }
}
*/

/**************************************************************************
                                                                          
  FUNCTION   :  DrawSelectFill(HDC hdc, BOOL fDraw)
                                                                          
  PURPOSE    :  Draws the selected clip rectangle with its dimensions on  
                the DC/screen                                             
                                                                          
***************************************************************************/   
/*
void DrawSelectFill( hdc, fDraw )
HDC     hdc;
BOOL    fDraw;
{
    if ( !IsRectEmpty( &rcClip ) ) 
    {
//        /* If a rectangular clip region has been selected, draw it
        PatBlt( hdc, rcClip.left, rcClip.top, 
                rcClip.right - rcClip.left,
                rcClip.bottom - rcClip.top,DSTINVERT);
    }
}
*/


/**************************************************************************
                                                                          
  FUNCTION   : DisplayPage( HWND hWnd )                                   
                                                                          
  PURPOSE    : Displays the image in the client window.                   
                                                                          
***************************************************************************/

void DisplayPage( HWND hWnd )
{                                  
	char					sOpenError[200];
	unsigned short			nWidth;
	unsigned short			nHeight;   
	
	// Free previous image handle
	if ( ImageHandle >= 0 )
	{
		DecompClose( ImageHandle );
		ImageHandle = -1;
	}
	
    hCursor = SetCursor( LoadCursor( NULL, IDC_WAIT ) );
    ShowCursor( TRUE );
    ImageHandle = DecompOpenTIFF( (LPSTR) OpenName, STATIC );
    ShowCursor( FALSE );
    SetCursor( hCursor );

    if ( ImageHandle < 0 )
    {
//		wsprintf( str, "Error opening file %s", OpenName );
//		MessageBox( hWnd, str, "Error", MB_OK );
        strcpy( sOpenError, "Problem! The requested map could not be found." );
        *OpenName = '\0';
        MessageBox( hWnd, sOpenError, "Error", MB_OK );
		// EnableMenuItem( hMenu, IDM_NEXTSHEET, MF_GRAYED | MF_BYCOMMAND );
		// EnableMenuItem( hMenu, IDM_PREVSHEET, MF_GRAYED | MF_BYCOMMAND );
		// EnableMenuItem( hMenu, IDM_NEXTPAGE, MF_GRAYED | MF_BYCOMMAND );
		// EnableMenuItem( hMenu, IDM_PREVPAGE, MF_GRAYED | MF_BYCOMMAND );
        return;
    }

	/*
    SelectImageSC = SelectImageEC = 0;
    SelectImageSR = SelectImageER = 0;
    rcPrint.bottom = 0;
    rcPrint.right = 0;
    rcPrint.top = 0;
    rcPrint.left = 0;
    */
// 	DecompGetImage( ImageHandle, (struct ImageAttributes far *)Image );
    GetClientRect( hWnd, &XRect );
	 nHeight = XRect.bottom;
	 nWidth = XRect.right;
                                    
    DecompOutput( ImageHandle, OUTPUTWINDOWS, (int)hWnd, 0, 0 );
    DecompOutputGreyScale( ImageHandle, OUTPUTWINDOWSGREY, (int)hWnd, 0, 0 );  
    
    if ( nRotate == ROT_UPRIGHT )
    	DecompRotation( ImageHandle, ROTATION_0 );
    else if ( nRotate == ROT_RIGHT )
    	DecompRotation( ImageHandle, ROTATION_90 );
    else if ( nRotate == ROT_LEFT )
    	DecompRotation( ImageHandle, ROTATION_270 );
    else if ( nRotate == ROT_UPSIDEDOWN )
    	DecompRotation( ImageHandle, ROTATION_180 );
    
    // GetClientRect( hWnd, &XRect );
    DecompSetViewRect( ImageHandle, 0, 0, XRect.right, XRect.bottom );
    DecompEOLFill( ImageHandle,WHITE );                       
    DecompViewRect( ImageHandle,FITHORZ );

    if ( nDisplay == DIS_UPPER_LEFT )
		DecompZoomRect( ImageHandle, 0, 0, ( nWidth / 2 ), ( nHeight / 2 ) );
    else if ( nDisplay == DIS_UPPER_RIGHT )
		DecompZoomRect( ImageHandle, ( nWidth / 2 ), 0, nWidth, ( nHeight / 2 ) );
    else if ( nDisplay == DIS_LOWER_LEFT )
		DecompZoomRect( ImageHandle, 0, ( nHeight / 2 ), ( nWidth / 2 ), nHeight );
    else if ( nDisplay == DIS_LOWER_RIGHT )
		DecompZoomRect( ImageHandle, ( nWidth / 2 ), ( nHeight / 2 ), nWidth, nHeight );

    InvalidateRect( hWnd, NULL, TRUE );
    // ResetScrollBars( ImageHandle,hWnd );
    wsprintf( str, "Image file - %s  ", (LPSTR)OpenName );
    SetWindowText( hWnd, str );
}



/*************************************************************************
                                                                          
  FUNCTION   : ReadFile(hwnd)                                             
                                                                          
  PURPOSE    : Called in response to a File/Open menu selection. It asks  
               the user for a file name and responds appropriately.       
                                                                          
**************************************************************************/

VOID FAR PASCAL ReadFile( HWND hwnd )
{
    char            szFile[FBUFSIZE];
    OPENFILENAME    of;


    // OpenName[0] = 0;
    lstrcpy( szFile, "*.*" );
    memset(&of,0x00,sizeof(OPENFILENAME));

    of.lStructSize  = sizeof(OPENFILENAME);
    of.hwndOwner    = hwnd;
    of.lpstrFilter  = 
        (LPSTR) "All Files (*.*)\0*.*\0TIFF Files (*.TIF)\0*.TIF\0";
    of.lpstrCustomFilter = NULL;
    of.nFilterIndex = 1;
    of.lpstrFile    = (LPSTR) szFile;
    of.nMaxFile     = sizeof(szFile);
    of.lpstrInitialDir = NULL;
    of.lpstrTitle   = NULL;
    of.Flags        = OFN_FILEMUSTEXIST;
    of.lpstrDefExt  = NULL;

    if ( !GetOpenFileName( &of ) )
        return;

    strcpy( OpenName, szFile );                     
//     PrintSelection = 0;
//     PrintScaling = 0;
}

/***************************************************************************

    FUNCTION:   SetMenuOptions                                                        
                                                                     
    PURPOSE:    Handle browse menu 
                
***************************************************************************/
/*
void SetMenuOptions( )
{          
	char		work[68];			/* Work variable
	char		*p;					/* Work variable 
	char		*r;					/* Work variable 
	BOOL		found = FALSE;		/* Work variable       
	short		i;					/* Work variable
	
//	/* Default case
	EnableMenuItem( hMenu, IDM_NEXTSHEET, MF_GRAYED | MF_BYCOMMAND );
	EnableMenuItem( hMenu, IDM_PREVSHEET, MF_GRAYED | MF_BYCOMMAND );
	EnableMenuItem( hMenu, IDM_NEXTPAGE, MF_GRAYED | MF_BYCOMMAND );
	EnableMenuItem( hMenu, IDM_PREVPAGE, MF_GRAYED | MF_BYCOMMAND );

//	EnableMenuItem( hMenu, IDM_PRINT, MF_ENABLED | MF_BYCOMMAND ); 

//	/* If no file, no menu options
	if ( OpenName[0] == 0 )
		return;
		
//	/* See if a drive is already there
	if ( OpenName[1] != ':' )		
	{
//		/* No drive; find the file
		strcpy( work, OpenName );
		strcpy( OpenName, "z:" );
		strcat( OpenName, work );             

		for ( p = drives ; *p ; p++ )
		{
			if ( *p == '-' )	//	/* Invalid drive...
				continue;
				
			OpenName[0] = *p;
			if ( access( OpenName, 0 ) == 0 )     
			{
				found = TRUE;
				break;       
			}
		}
		
//		/* Did we find the file? If not, just exit...
		if ( found == FALSE )
			return;		
	}
	
//	/* If file is not of correct format, no menu options 
	p = OpenName + strlen(OpenName) - 3; 
	if ( ( *p != '.' ) || ( !isdigit(*(p+1)) ) || ( !isdigit(*(p+2)) ) )
		return;
		
//	/* Verify next sheet
	strcpy( work, OpenName );
	p = work + strlen(work) - 1;           
	for ( i = 0 ; i < SKIPLIMIT ; i++ )
	{
		if ( ( *(p-1) == '9' ) && ( *p == '9' ) )
			break;
		else if ( *p != '9' )
			(*p)++;
		else        
		{
			*p = '0';
			(*(p-1))++;
		}
		if ( access( work, 0 ) == 0 )                
		{
			EnableMenuItem( hMenu, IDM_NEXTSHEET, MF_ENABLED | MF_BYCOMMAND );
			strcpy( NextSheet, work );
			break;
		}
	}
	
//	/* Verify previous sheet 
	strcpy( work, OpenName );
	p = work + strlen(work) - 1;
	for ( i = 0 ; i < SKIPLIMIT ; i++ )
	{
		if ( ( *(p-1) == '0' ) && ( *p == '0' ) )
			break;
		else if ( *p != '0' )
			(*p)--;
		else        
		{
			*p = '9';         
			(*(p-1))--;
		}
		if ( access( work, 0 ) == 0 )
		{
			EnableMenuItem( hMenu, IDM_PREVSHEET, MF_ENABLED | MF_BYCOMMAND );
			strcpy( PrevSheet, work );
			break;
		}
	}                                      
	
//	/* Verify next page   
//	/* Set extension to '00'; add 1 to file prefix; test
	strcpy( work, OpenName );
	p = work + strlen(work) - 1;
	*p = '0';
	p--;
	*p = '0';
	p--;
	p--;                                       
	if ( isdigit( *p ) )
	{
		for ( i = 0, r = p ; i < SKIPLIMIT ; i++ )
		{
			if ( *p !='9' )
				(*p)++;
			else if ( ( r != work ) && isdigit(*--r) )
			{
				if ( *r != '9' )
				{
					*p = '0';
					(*r)++;
				}
				else
				{
					*r = '0';
					continue;
				}   
			}
			else
				break;
			if ( access( work, 0 ) == 0 )
			{
				EnableMenuItem( hMenu, IDM_NEXTPAGE, MF_ENABLED | MF_BYCOMMAND );
				strcpy( NextPage, work );
				break;
			}
		}
	}
		
//	/* Verify previous page 
//	/* Set extension to '00'; subtract 1 to file prefix; test 
	strcpy( work, OpenName );
	p = work + strlen(work) - 1;
	*p = '0';
	p--;
	*p = '0';
	p--;
	p--;
	if ( isdigit( *p ) )
	{
		for ( i = 0, r = p ; i < SKIPLIMIT ; i++ )
		{
			if ( *p !='0' )
				(*p)--;
			else if ( ( r != work ) && isdigit(*--r) )
			{
				if ( *r != '0' )
				{
					*p = '9';
					(*r)--;
				}
				else
				{
					*r = '9';
					continue;
				}
			}
			else
				break;
			if ( access( work, 0 ) == 0 )
			{
				EnableMenuItem( hMenu, IDM_PREVPAGE, MF_ENABLED | MF_BYCOMMAND );
				strcpy( PrevPage, work );
				break;
			}
		}
	} 
}
*/

/****************************************************************************

    FUNCTION:   Rename(HWND, unsigned, WPARAM, LPARAM)

    PURPOSE:    Processes messages for "Rename" dialog box

    MESSAGES:

    WM_INITDIALOG - initialize dialog box
    WM_COMMAND    - Input received

****************************************************************************/

BOOL FAR PASCAL RenameFile(
HWND        hDlg,           /* Window handle of the dialog box */
unsigned    message,        /* Type of message                 */
WPARAM      wParam,         /* Message-specific information    */
LPARAM      lParam
)
{
	static char		FileBuf[FBUFSIZE];
	int				errorCode;
	char			szTemp[80]; 
	unsigned long	MapNum;
	int				digits;

	char	renFileName[12];
	char	renFileExt[6];
	char	renPath[FBUFSIZE];

	switch( message )
	{
	case WM_INITDIALOG:                      /* message: intialize dialog box */
		strcpy( FileBuf, FileExt );
//		DlgDirList( hDlg, FileBuf, LB_RESTORE, 
//	   		ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
		FillDirListBox( hDlg, FileBuf );
		SetFocus( GetDlgItem( hDlg, ID_FILENAME ) );
		SendDlgItemMessage( hDlg, ID_FILENAME, EM_LIMITTEXT, 15, 0L );
		SendDlgItemMessage( hDlg, ID_FILEEXT,  EM_LIMITTEXT, 3, 0L );
		SendDlgItemMessage( hDlg, LB_RESTORE,  LB_SETCURSEL, 0, 0L );
		EnableWindow( GetDlgItem( hDlg, ID_NEWDOC ), bAutoNum );

#ifdef WIN32
		if ( DlgDirSelectEx( hDlg, FileBuf, sizeof(FileBuf), LB_RESTORE ) == 0 )
#else
		if ( DlgDirSelect( hDlg, FileBuf, LB_RESTORE ) == 0 )
#endif
		{
			strcpy( OpenName, FileBuf );
			DisplayPage( hWndMain );
		//  SendMessage(hWndMain, WM_PAINT, 0, 0L);
		}
		return( FALSE );
		break;

	case WM_COMMAND:                           /* message : recived a command */
		switch( LOWORD(wParam) )
		{
		case IDOK:
#ifdef WIN32
			if ( DlgDirSelectEx( hDlg, FileBuf, sizeof(FileBuf), LB_RESTORE ) != 0 )
#else
			if ( DlgDirSelect( hDlg, FileBuf, LB_RESTORE ) != 0 )
#endif
			{
				strcat( FileBuf, FileExt );
//				DlgDirList( hDlg, FileBuf, LB_RESTORE,
//					ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
				FillDirListBox( hDlg, FileBuf );
				break;
			}
               
			strcpy( OpenName, FileBuf );
			GetDlgItemText( hDlg, ID_FILENAME, renFileName, sizeof(renFileName) - 1 );
			GetDlgItemText( hDlg, ID_FILEEXT, renFileExt, sizeof(renFileExt) - 1 );
			strcpy( renPath, renFileName );
			strcat( renPath, "." );
			strcat( renPath, renFileExt );
			errorCode = rename( OpenName, renPath );
			if ( errorCode )
			{
				MessageBox( hDlg, "Error Renaming TIFF File !", 
					"Error", MB_OK | MB_ICONEXCLAMATION );
			}
			else
			{
				strcpy( OpenName, renPath );
//				DlgDirList( hDlg, FileExt, LB_RESTORE, 
//					ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
				FillDirListBox( hDlg, FileExt );
				SendDlgItemMessage( hDlg, LB_RESTORE,  LB_SETCURSEL, 0, 0L );
#ifdef WIN32
				if ( DlgDirSelectEx( hDlg, FileBuf, sizeof(FileBuf), LB_RESTORE ) == 0 )
#else
				if ( DlgDirSelect( hDlg, FileBuf, LB_RESTORE ) == 0 )
#endif
				{
					strcpy( OpenName, FileBuf );        
					DisplayPage( hWndMain );
					// SendMessage( hWndMain, WM_PAINT, 0, 0L );

					if ( nDocType == TYP_MAPS )
					{
						GetDlgItemText( hDlg, ID_FILENAME, renFileName, sizeof(renFileName) - 1 );
						GetDlgItemText( hDlg, ID_FILEEXT, renFileExt, sizeof(renFileExt) - 1 );
						
						/*
						MapNum = atol( renFileName );
						MapNum++;         
						digits = strlen( renFileName );
						switch( digits )
						{
						case 1:
							wsprintf( szTemp, "%01ld", MapNum );
							break;
						case 2:
							wsprintf( szTemp, "%02ld", MapNum );
							break;
						case 3:
							wsprintf( szTemp, "%03ld", MapNum );
							break;
						case 4:
							wsprintf( szTemp, "%04ld", MapNum );
							break;
						case 5:
							wsprintf( szTemp, "%05ld", MapNum );
							break;
						case 6:
							wsprintf( szTemp, "%06ld", MapNum );
							break;
						case 7:
							wsprintf( szTemp, "%07ld", MapNum );
							break;
						case 8:
							wsprintf( szTemp, "%08ld", MapNum );
							break;
						}		
						*/
						
						if ( bAutoNum == FALSE )
							strcpy( szTemp, "" );				
						else if ( IncrementMap( szTemp, renFileName ) == FALSE )
							strcpy( szTemp, "" );	
						SetDlgItemText( hDlg, ID_FILENAME, szTemp );
						
						digits = strlen( renFileExt );
						switch( digits )
						{
						case 1:
							wsprintf( szTemp, "%01d", 0 );
							break;
						case 2:
							wsprintf( szTemp, "%02d", 0 );
							break;
						case 3:
							wsprintf( szTemp, "%03d", 0 );
							break;
						}
						SetDlgItemText( hDlg, ID_FILEEXT, szTemp );
						SetFocus( GetDlgItem( hDlg, ID_FILENAME ) );
					}

					else
					{
						MapNum = atol( renFileExt );
						MapNum++;         
						digits = strlen( renFileExt );
						switch( digits )
						{
						case 1:
							wsprintf( szTemp, "%01ld", MapNum );
							break;
						case 2:
							wsprintf( szTemp, "%02ld", MapNum );
							break;
						case 3:
							wsprintf( szTemp, "%03ld", MapNum );
							break;
						/*
                    	case 4:
                    		wsprintf( szTemp, "%04ld", MapNum );
                    		break;
                    	case 5:
                    		wsprintf( szTemp, "%05ld", MapNum );
                    		break;
                    	case 6:
                    		wsprintf( szTemp, "%06ld", MapNum );
                    		break;
                    	case 7:
                    		wsprintf( szTemp, "%07ld", MapNum );
                    		break;
                    	case 8:
                    		wsprintf( szTemp, "%08ld", MapNum );
                    		break;
						*/
						}

						if ( bAutoNum == FALSE )
							strcpy( szTemp, "" );				
						SetDlgItemText( hDlg, ID_FILEEXT, szTemp );
						SetFocus( GetDlgItem( hDlg, ID_FILEEXT ) );
					}
				}
			}
			break;

		case ID_NEWDOC:
#ifdef WIN32
			if ( DlgDirSelectEx( hDlg, FileBuf, sizeof(FileBuf), LB_RESTORE ) != 0 )
#else
			if ( DlgDirSelect( hDlg, FileBuf, LB_RESTORE ) != 0 )
#endif
			{
				strcat( FileBuf, FileExt );
//				DlgDirList( hDlg, FileBuf, LB_RESTORE,
//					ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
				FillDirListBox( hDlg, FileBuf );
				break;
			}                                               
				
			strcpy( OpenName, FileBuf );
			GetDlgItemText( hDlg, ID_FILENAME, renFileName, sizeof(renFileName) - 1 );
			GetDlgItemText( hDlg, ID_FILEEXT, renFileExt, sizeof(renFileExt) - 1 );
			
			/*
			MapNum = atol(renFileName);           
			if ( MapNum == 0 )
				break;
				
			MapNum++;         
			digits = strlen( renFileName );
			switch( digits )
			{
			case 1:
				wsprintf( szTemp, "%01ld", MapNum );
				break;
			case 2:
				wsprintf( szTemp, "%02ld", MapNum );
				break;
			case 3:
				wsprintf( szTemp, "%03ld", MapNum );
				break;
			case 4:
				wsprintf( szTemp, "%04ld", MapNum );
				break;
			case 5:
				wsprintf( szTemp, "%05ld", MapNum );
				break;
			case 6:
				wsprintf( szTemp, "%06ld", MapNum );
				break;
			case 7:
				wsprintf( szTemp, "%07ld", MapNum );
				break;
			case 8:
				wsprintf( szTemp, "%08ld", MapNum );
				break;
			}						
            */
            
			if ( bAutoNum == FALSE )
				strcpy( szTemp, "" );	
			else if ( IncrementMap( szTemp, renFileName ) == FALSE )
				strcpy( szTemp, "" );	
						
			SetDlgItemText( hDlg, ID_FILENAME, szTemp );

			digits = strlen( renFileExt );
			switch( digits )
			{
			case 1:
				wsprintf( szTemp, "%01d", 0 );
				break;
			case 2:
				wsprintf( szTemp, "%02d", 0 );
				break;
			case 3:
				wsprintf( szTemp, "%03d", 0 );
				break;
			}
			SetDlgItemText( hDlg, ID_FILEEXT, szTemp );
			SetFocus( GetDlgItem( hDlg, ID_FILENAME ) );
			break;

		case LB_RESTORE:
#ifdef WIN32
			if ( ( HIWORD(wParam) != 1 ) &&	/* Single click on item */
				( HIWORD(wParam) != 2 ) )	/* Double click on item */
				break;						/* Fall thru otherwise */
#else
			if ( ( HIWORD(lParam) != 1 ) &&	/* Single click on item */
				( HIWORD(lParam) != 2 ) )	/* Double click on item */
				break;						/* Fall thru otherwise */
#endif

		case ID_DISPLAY:
         if (bSaveMap)
         {
            // Save current image to OpenName
            char szNewName[_MAX_PATH];  
            sprintf(szNewName, "%s\\NeedRotate\\%s", SaveImgDir, OpenName);
            CopyFile(OpenName, szNewName, false);
            bSaveMap = false;
         }

#ifdef WIN32
			if ( DlgDirSelectEx( hDlg, FileBuf, sizeof(FileBuf), LB_RESTORE ) != 0 )
#else
			if ( DlgDirSelect ( hDlg, FileBuf, LB_RESTORE ) != 0 )
#endif
			{
				strcat(	FileBuf, FileExt );
//				DlgDirList( hDlg, FileBuf, LB_RESTORE,
//					ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
				FillDirListBox( hDlg, FileBuf );
				break;
			}

			//EndDialog(hWnd, TRUE);
			strcpy( OpenName, FileBuf );   
			DisplayPage( hWndMain );
			// SendMessage( hWndMain, WM_PAINT, 0, 0L );
			// return(TRUE);
			break;                                             
		
		case IDC_PARAMETERS:
        	if ( DialogBox( hInst, "Parameters", hDlg, Parameters ) == TRUE )
				DisplayPage( hWndMain );
			strcpy( FileBuf, FileExt );
//			DlgDirList( hDlg, FileBuf, LB_RESTORE, 
//	   			ID_RESTOREPATH, DDL_EXCLUSIVE | DDL_ARCHIVE );
			FillDirListBox( hDlg, FileBuf );
			EnableWindow( GetDlgItem( hDlg, ID_NEWDOC ), bAutoNum );
			SendDlgItemMessage( hDlg, LB_RESTORE,  LB_SETCURSEL, 0, 0L );
			SetFocus( GetDlgItem( hDlg, ID_FILENAME ) );
	    	break;

		case IDCANCEL:
			ImageHandle = -1;
			*OpenName = 0;
			InvalidateRect( hWndMain, NULL, TRUE );
			SendMessage( hWndMain, WM_PAINT, 0, 0L );
			EndDialog( hDlg, FALSE );
			return( FALSE );
			break;

      case ID_BADMAP:
         // Copy file to specific folder
         char szNewName[_MAX_PATH];
         sprintf(szNewName, "%s\\BadMaps\\%s", SaveImgDir, OpenName);
         CopyFile(OpenName, szNewName, false);
         break;

      case ID_ROTATE:
         GetClientRect( hWndMain, &XRect );
    	   DecompRotation( ImageHandle, ROTATE_RIGHT );
	      InvalidateRect(hWndMain, NULL, FALSE );
         bSaveMap = TRUE;
         break;
		}                                           /* End Command Switch */

	default:
		return( FALSE );
		break;
	}                                               /* End Main Switch */
}

/****************************************************************************

    FUNCTION:   BOOL IncrementMap( char *sOld, char *snew )

    PURPOSE:    Increments map number

    RETURNS:	TRUE			Everything OK    
    			FALSE			Map could not be incremented

****************************************************************************/

BOOL IncrementMap( char *sNew, char *sOld )
{
	BOOL		bResult = FALSE;		// Result of this routine     
	short		nLen;					// Length of old file name    
	char		*p;						// Work variable

	strcpy( sNew, sOld );	
	nLen = strlen( sOld );           
	if ( nLen == 0 )					// Handle case of empty string
		return( bResult );

	// Increment numbers from back of string		
	for ( p = sNew + nLen - 1 ; p >= sNew ; p-- )
	{
		if ( isdigit(*p) == FALSE )		// Hit an alpha; no valid value
			return( FALSE );
		else if ( *p == '9' )			// Hit a 9; make it a 0 and keep going
			*p = '0';
		else							// Not a 9; increment and quit
		{
			(*p)++;
			return( TRUE );
		}
	}

	// Old file was '999...'	
	return( FALSE );
}		


/****************************************************************************

    FUNCTION:   BOOL FillDirListBox( HWND hDlg, char * pFilter )

    PURPOSE:    Fills a list box with the long filename

    RETURNS:	TRUE			Everything OK    
    			FALSE			Could not fill

****************************************************************************/

BOOL FillDirListBox( HWND hDlg, char * pFilter )
{
	char *pFilepart = NULL;
	HANDLE hFile = NULL;
#ifdef WIN32
	char path[FBUFSIZE];
	WIN32_FIND_DATA FData;
#endif

	BOOL ret = DlgDirList( hDlg, pFilter, LB_RESTORE, ID_RESTOREPATH,
		DDL_EXCLUSIVE | DDL_ARCHIVE ) != 0;

#ifdef WIN32
	// Scan list and convert short names to long names
	int count = SendDlgItemMessage( hDlg, LB_RESTORE, LB_GETCOUNT, 0, 0 );
	for (int i=0; ret && i<count; i++)
	{
		SendDlgItemMessage( hDlg, LB_RESTORE, LB_GETTEXT, i, (LPARAM)path);

		hFile = FindFirstFile( path, &FData );
		if ( hFile != INVALID_HANDLE_VALUE &&
			 strlen( FData.cFileName ) > 0 &&
			 _stricmp( path, FData.cFileName ) != 0 )
		{
			SendDlgItemMessage( hDlg, LB_RESTORE, LB_DELETESTRING, i, 0L);
			SendDlgItemMessage( hDlg, LB_RESTORE, LB_INSERTSTRING, i, (LPARAM)FData.cFileName);
		}
	}
#endif	

	return ret;
}
