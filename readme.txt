Kirk Chao 2/12/1999

This project contains the following files:

- All source needed to build Rentiff 32-bits

- Compiled Rentiff.exe (32-bit Release)
  Note: this was built using Microsoft Visual C++ 6.0 (SP2)
  If using an earlier version, open the included .MAK file
  to create new project files.

- DecompNT.DLL and .LIB (32-bit)

- Decomp.DLL and .LIB (16-bit)

- Test Tiff files