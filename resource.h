//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Rentiff.rc
//
#define IDC_PARAMETERS                  3
#define IDM_EXIT                        16
#define IDD_ADVPRINT                    101
#define IDM_FULLMAP                     101
#define IDD_DIALOG1                     102
#define IDM_NEXTSHEET                   102
#define IDM_PREVSHEET                   103
#define IDM_NEXTPAGE                    104
#define IDM_PREVPAGE                    105
#define IDI_ICON1                       106
#define IDM_PARAMETERS                  106
#define IDD_PARAMETERS                  107
#define IDC_PRINTPREVIEW                1000
#define IDC_PRINTSETUP                  1001
#define IDC_AREA                        1002
#define IDC_ENTIRE                      1003
#define IDC_SELECTION                   1004
#define IDC_FRAME1                      1005
#define IDC_DISPLAY1                    1007
#define IDC_DISPLAY2                    1008
#define IDC_DISPLAY3                    1009
#define IDC_SCALING                     1010
#define IDC_DISPLAY4                    1010
#define IDC_SCALE                       1011
#define IDC_DISPLAY5                    1011
#define IDC_ORIGINAL                    1012
#define IDC_DOCTYPE1                    1012
#define IDC_DOCTYPE2                    1013
#define IDC_FRAME2                      1014
#define IDC_ROTATION                    1015
#define IDC_ROTATION1                   1016
#define IDC_ROTATION3                   1017
#define IDC_ROTATION2                   1018
#define IDC_ROTATION4                   1019
#define LB_RESTORE                      1020
#define ID_FILENAME                     1021
#define ID_FILEEXT                      1022
#define ID_DISPLAY                      1023
#define ID_ZOOM1                        1024
#define ID_ROTATE                       1024
#define ID_FILEMASK                     1025
#define ID_ROTATE2                      1025
#define ID_BADMAP                       1025
#define ID_NEWDOC                       1026
#define ID_RESTOREPATH                  1027
#define ID_AUTONUM                      1028
#define ID_VERSION                      1029
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         107
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
